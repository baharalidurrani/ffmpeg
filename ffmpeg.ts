// var FFmpeg = require("fluent-ffmpeg");
import FFmpeg from "fluent-ffmpeg";

export function removeAudio(source, destination) {
  FFmpeg({ source: source })
    .withNoAudio()
    .saveToFile(destination)
    .on("start", function(commandLine) {
      console.log("Spawned FFmpeg with command: " + commandLine);
    })

    .on("codecData", function(data) {
      console.log(
        "Input is " + data.audio + " audio with " + data.video + " video"
      );
    })

    .on("progress", function(progress) {
      console.log(progress);
    })

    .on("error", function(err) {
      console.log("Cannot process video: " + err.message);
    })

    .on("end", function() {
      console.log("Processing finished successfully");
    });
}

//////////////////////////////////////////

export function combineVideos(source1, source2, destination) {
  FFmpeg()
    .addInput(source1)
    .addInput(source2)
    .mergeToFile(destination)
    .on("start", function(commandLine) {
      console.log("Spawned FFmpeg with command: " + commandLine);
    })

    .on("codecData", function(data) {
      console.log(
        "Input is " + data.audio + " audio with " + data.video + " video"
      );
    })

    .on("progress", function(progress) {
      console.log(progress);
    })

    .on("error", function(err) {
      console.log("Cannot process video: " + err.message);
    })

    .on("end", function() {
      console.log("Processing finished successfully");
    });
}

//////////////////////////////////////////

export function addAudio(video, audio, destination) {
  //  ffmpeg -i 2mb.mp4 -i audio1mb.mp3 -shortest -c copy -map 0:v:0 -map 1:a:0 addaudio.mp4
  FFmpeg()
    .addInput(video)
    .addInput(audio)
    .outputOptions(["-shortest", "-c", "copy", "-map 0:v:0", "-map 1:a:0"])
    .saveToFile(destination)
    .on("start", function(commandLine) {
      console.log("Spawned FFmpeg with command: " + commandLine);
    })

    .on("codecData", function(data) {
      console.log(
        "Input is " + data.audio + " audio with " + data.video + " video"
      );
    })

    .on("progress", function(progress) {
      console.log(progress);
    })

    .on("error", function(err) {
      console.log("Cannot process video: " + err.message);
    })

    .on("end", function() {
      console.log("Processing finished successfully");
    });
}

/////////////////////////////////////////

export function addImage(file1, file2, destination, start, end) {
  FFmpeg()
    .input(file1) //video
    .input(file2) //watermark
    .videoCodec("libx264")
    // .outputOptions("-pix_fmt yuv420p")
    // .complexFilter(["overlay=x=(main_w-overlay_w)/2:y=(main_h-overlay_h)/2:enable='between(t,2,5)'"])
    .complexFilter(
      [
        {
          filter: "overlay",
          options: {
            x: "(main_w-overlay_w)/2",
            y: "(main_h-overlay_h)/2",
            enable: "between(t," + start + "," + end + ")"
          }
        }
      ],
      null
    )
    .saveToFile(destination)
    .on("start", function(commandLine) {
      console.log("Spawned FFmpeg with command: " + commandLine);
    })

    .on("codecData", function(data) {
      console.log(
        "Input is " + data.audio + " audio with " + data.video + " video"
      );
    })

    .on("progress", function(progress) {
      console.log(progress);
    })

    .on("error", function(err) {
      console.log("Cannot process video: " + err.message);
    })

    .on("end", function() {
      console.log("Processing finished successfully");
    });
}
export function videoOverlay(file1, file2, destination) {
  FFmpeg()
    .input(file1) //video
    .input(file2) //watermark video must have alpha channel
    .videoCodec("libx264")
    // .outputOptions("-pix_fmt yuv420p")
    .complexFilter(
      [
        {
          filter: "overlay"
        }
      ],
      null
    )
    .output(destination)
    .on("start", function(commandLine) {
      console.log("Spawned FFmpeg with command: " + commandLine);
    })
    .on("codecData", function(data) {
      console.log(
        "Input is " + data.audio + " audio with " + data.video + " video"
      );
    })
    .on("progress", function(progress) {
      console.log(progress);
    })
    .on("error", function(err) {
      console.log("Cannot process video: " + err.message);
    })
    .on("end", function() {
      console.log("Processing finished successfully");
    })
    .run();
}

/////////////////////////////////////////

// problem with start-end time
// problem with center positioning
export function addText(video, text, destination, start, end) {
  // ffmpeg -i D:\Videos\city.mp4 -filter_complex "[0:v]drawtext=fontfile='C\:\\Windows\\Fonts\\arial.ttf':text='A Celebration of Life':fontsize=64:fontcolor=white" D:\Videos\text.mp4
  // ffmpeg -i 2mb.mp4 -filter_complex "[0:v]drawtext=text='A Celebration of Life':fontsize=64:fontcolor=white" outtext.mp4
  FFmpeg()
    .input(video) //video
    .complexFilter(
      [
        {
          filter: "drawtext",
          options: {
            text: text,
            fontsize: 64,
            fontcolor: "white",
            x: "(main_w-text_w)/2",
            y: "(main_h-text_h)/2",
            enable: "between(t," + start + "," + end + ")"
          }
        }
      ],
      null
    )
    .saveToFile(destination)
    .on("start", function(commandLine) {
      console.log("Spawned FFmpeg with command: " + commandLine);
    })

    .on("codecData", function(data) {
      console.log(
        "Input is " + data.audio + " audio with " + data.video + " video"
      );
    })

    .on("progress", function(progress) {
      console.log(progress);
    })

    .on("error", function(err) {
      console.log("Cannot process video: " + err.message);
    })

    .on("end", function() {
      console.log("Processing finished successfully");
    });
}

/////////////////////////////////////////

export function trimVideo(video, startTime, duration, destination) {
  FFmpeg()
    .input(video) //video
    .setStartTime(startTime)
    .setDuration(duration)
    .saveToFile(destination)
    .on("start", function(commandLine) {
      console.log("Spawned FFmpeg with command: " + commandLine);
    })

    .on("codecData", function(data) {
      console.log(
        "Input is " + data.audio + " audio with " + data.video + " video"
      );
    })

    .on("progress", function(progress) {
      console.log(progress);
    })

    .on("error", function(err) {
      console.log("Cannot process video: " + err.message);
    })

    .on("end", function() {
      console.log("Processing finished successfully");
    });
}
/////////////////////////////////////////

export function resizingVideo(video, size, destination) {
  FFmpeg()
    .input(video)
    .size(size)
    .saveToFile(destination)
    .on("start", function(commandLine) {
      console.log("Spawned FFmpeg with command: " + commandLine);
    })
    .on("codecData", function(data) {
      console.log(
        "Input is " + data.audio + " audio with " + data.video + " video"
      );
    })
    .on("progress", function(progress) {
      console.log(progress);
    })
    .on("error", function(err) {
      console.log("Cannot process video: " + err.message);
    })
    .on("end", function() {
      console.log("Processing finished successfully");
    });
}
/////////////////////////////////////////

export function aspectVideo(video, size, aspect, destination) {
  FFmpeg()
    .input(video)
    .size(size)
    .aspect(aspect)
    .autoPad(true, "pink")
    .saveToFile(destination)
    .on("start", function(commandLine) {
      console.log("Spawned FFmpeg with command: " + commandLine);
    })
    .on("codecData", function(data) {
      console.log(
        "Input is " + data.audio + " audio with " + data.video + " video"
      );
    })
    .on("progress", function(progress) {
      console.log(progress);
    })
    .on("error", function(err) {
      console.log("Cannot process video: " + err.message);
    })
    .on("end", function() {
      console.log("Processing finished successfully");
    });
}

/////////////////////////////////////////////////
// ffmpeg -i input.mp4 -filter_complex "[0:v]setpts=0.5*PTS[v];[0:a]atempo=2.0[a]" -map "[v]" -map "[a]" output.mp4
//Spawned FFmpeg with command: ffmpeg -i ./Film.mp4 -y -filter:a atempo=2.0 -filter:v setpts=0.5*PTS ./output.mp4
interface speedEnum {
  source: number;
  videoSpeed: number;
  audio1: number;
  audio2: number;
}

export function videoSpeed(video: string, speed: number, destination: string) {
  let speedEnum: Array<speedEnum>;
  speedEnum = [
    { source: 0.25, videoSpeed: 4.0, audio1: 0.5, audio2: 0.5 },
    { source: 0.5, videoSpeed: 2.0, audio1: 0.5, audio2: 1 },
    { source: 2.0, videoSpeed: 0.5, audio1: 2, audio2: 1 },
    { source: 4.0, videoSpeed: 0.25, audio1: 2, audio2: 2 }
  ];
  function initSpeeds(speedObj: speedEnum) {
    return speedObj.source === speed;
  }
  var speedObj = speedEnum.find(initSpeeds);

  FFmpeg()
    .input(video) //video
    .videoFilters(`setpts=${speedObj.videoSpeed}*PTS`)
    .audioFilters(`atempo=${speedObj.audio1}`)
    //The atempo filter is limited to using values between 0.5 and 2.0 That's why chaining two filters to achieve 0.25 and 4.0
    .audioFilters(`atempo=${speedObj.audio2}`)
    // .audioFilter({
    //   filter: "asetpts",
    //   options: `${speed}*PTS`
    // })
    .output(destination)
    .on("start", function(commandLine) {
      console.log("Spawned FFmpeg with command: " + commandLine);
    })
    .on("codecData", function(data) {
      console.log(
        "Input is " + data.audio + " audio with " + data.video + " video"
      );
    })
    .on("progress", function(progress) {
      console.log(progress);
    })
    .on("error", function(err) {
      console.log("Cannot process video: " + err.message);
    })
    .on("end", function() {
      console.log("Processing finished successfully");
    })
    .run();
}

/////////////////////////////////////////////////
//ffmpeg -i input.mp4 -filter:v "crop=w:h:x:y" output.mp4
export function videoCrop(video, w, h, destination) {
  FFmpeg()
    .input(video) //video
    // .videoFilters("setpts=0.5*PTS")
    .videoFilters([
      {
        filter: "crop",
        options: { w, h } //options JSON with es6
      }
    ])
    .output(destination)
    .on("start", function(commandLine) {
      console.log("Spawned FFmpeg with command: " + commandLine);
    })
    .on("codecData", function(data) {
      console.log(
        "Input is " + data.audio + " audio with " + data.video + " video"
      );
    })
    .on("progress", function(progress) {
      console.log(progress);
    })
    .on("error", function(err) {
      console.log("Cannot process video: " + err.message);
    })
    .on("end", function() {
      console.log("Processing finished successfully");
    })
    .run();
}
export async function videoCropAndScale(
  video,
  newWidth,
  newHeight,
  destination
) {
  const { width, height } = await getDimentions(video);
  if ((width / height).toFixed(2) > (newWidth / newHeight).toFixed(2)) {
    // y=0 case
    const x = width - (newWidth / newHeight) * height;
    // newWidth = newWidth - x;
    console.log(`Old v  Old Res: ${width}x${height}`);
    console.log(`Ratio: ${width / height}`);
    console.log(`New Intrim Res: ${width - x}x${height}`);
    console.log(`Ratio: ${(width - x) / height}`);
  } else if ((width / height).toFixed(2) < (newWidth / newHeight).toFixed(2)) {
    // x=0 case
    // calculate or resize with padding or blur sides
    const y = height - (newHeight / newWidth) * width;
    console.log(`Old v  Old Res: ${width}x${height}`);
    console.log(`Ratio: ${width / height}`);
    console.log(`New Intrim Res: ${width}x${height - y}`);
    console.log(`Ratio: ${width / (height - y)}`);
  } else {
    console.log("Same Aspect Ratio forward for resizing");
    console.log(`Res: ${width}x${height}`);
    console.log(`Ratio: ${width / height}`);
  }
  // FFmpeg()
  //   .input(video) //video
  //   // .videoFilters("setpts=0.5*PTS")
  //   .videoFilters([
  //     {
  //       filter: "crop",
  //       options: { w, h } //options JSON with es6
  //     }
  //   ])
  //   .output(destination)
  //   .on("start", function(commandLine) {
  //     console.log("Spawned FFmpeg with command: " + commandLine);
  //   })
  //   .on("codecData", function(data) {
  //     console.log(
  //       "Input is " + data.audio + " audio with " + data.video + " video"
  //     );
  //   })
  //   .on("progress", function(progress) {
  //     console.log(progress);
  //   })
  //   .on("error", function(err) {
  //     console.log("Cannot process video: " + err.message);
  //   })
  //   .on("end", function() {
  //     console.log("Processing finished successfully");
  //   })
  //   .run();
}

/////////////////////////////////////////////////
//ffmpeg -i input.mp4 -filter:v "crop=w:h:x:y" output.mp4
export function videoFade(video, duration, destination) {
  FFmpeg()
    .input(video) //video
    // .videoFilters("setpts=0.5*PTS")
    .videoFilters([
      {
        filter: "fade",
        options: { type: "out", duration: duration, start_time: 5 } //options JSON with es6
      }
    ])
    .output(destination)
    .on("start", function(commandLine) {
      console.log("Spawned FFmpeg with command: " + commandLine);
    })
    .on("codecData", function(data) {
      console.log(
        "Input is " + data.audio + " audio with " + data.video + " video"
      );
    })
    .on("progress", function(progress) {
      console.log(progress);
    })
    .on("error", function(err) {
      console.log("Cannot process video: " + err.message);
    })
    .on("end", function() {
      console.log("Processing finished successfully");
    })
    .run();
}

/////////////////////////////////////////////////
export function videoBrightness(video, brightness = -0.1, destination) {
  FFmpeg()
    .input(video) //video
    .videoFilters([
      {
        filter: "eq",
        options: { brightness } //options JSON with es6
      }
    ])
    .output(destination)
    .on("start", function(commandLine) {
      console.log("Spawned FFmpeg with command: " + commandLine);
    })
    .on("codecData", function(data) {
      console.log(
        "Input is " + data.audio + " audio with " + data.video + " video"
      );
    })
    .on("progress", function(progress) {
      console.log(progress);
    })
    .on("error", function(err) {
      console.log("Cannot process video: " + err.message);
    })
    .on("end", function() {
      console.log("Processing finished successfully");
    })
    .run();
}

//////////////////////////////////////////////
//luma_radius=50:chroma_radius=25:luma_power=1

export function videoBlur(video, intensity = 10, destination) {
  FFmpeg()
    .input(video) //video
    .videoFilters([
      {
        filter: "boxblur",
        options: {
          luma_radius: intensity,
          chroma_radius: intensity,
          luma_power: 1
        } //options JSON with es6
      }
    ])
    .output(destination)
    .on("start", function(commandLine) {
      console.log("Spawned FFmpeg with command: " + commandLine);
    })
    .on("codecData", function(data) {
      console.log(
        "Input is " + data.audio + " audio with " + data.video + " video"
      );
    })
    .on("progress", function(progress) {
      console.log(progress);
    })
    .on("error", function(err) {
      console.log("Cannot process video: " + err.message);
    })
    .on("end", function() {
      console.log("Processing finished successfully");
    })
    .run();
}
export function support() {
  // FFmpeg.getAvailableFormats(function(err, formats) {
  //   console.log("Available formats:");
  //   console.dir(formats);
  // });

  // FFmpeg.getAvailableCodecs(function(err, codecs) {
  //   console.log("Available codecs:");
  //   console.dir(codecs);
  // });

  FFmpeg.getAvailableEncoders(function(err, encoders) {
    console.log("Available encoders:");
    console.dir(encoders);
  });

  // FFmpeg.getAvailableFilters(function(err, filters) {
  //   console.log("Available filters:");
  //   console.dir(filters);
  // });
}

export function getDimentions(video: string) {
  return new Promise<{ width: number; height: number }>((res, rej) => {
    FFmpeg.ffprobe(video, function(err, metadata) {
      if (err) {
        console.error(err);
        rej(err);
      } else {
        res({
          width: metadata.streams[0].width,
          height: metadata.streams[0].height
        });
      }
    });
  });
}

export function getMeta(video: string): Promise<FFmpeg.FfprobeData> {
  return new Promise<FFmpeg.FfprobeData>((res, rej) => {
    FFmpeg.ffprobe(video, function(err, metadata) {
      console.log("metadata-------------------------------------", metadata);
      console.log("metadata-------------------------------------");
      if (err) {
        console.error(err);
        rej(err);
      } else {
        res(metadata);
      }
    });
  });
}
