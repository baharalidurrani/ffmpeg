// var ffmpeg = require("./ffmpeg");
import {
  removeAudio,
  combineVideos,
  addAudio,
  addImage,
  addText,
  trimVideo,
  resizingVideo,
  aspectVideo,
  videoSpeed,
  videoCrop,
  videoFade,
  videoBrightness,
  videoBlur,
  videoOverlay,
  getDimentions,
  videoCropAndScale
} from "./ffmpeg";

// removeAudio("./media/source/2mb.mp4", "./media/destination/out5ts.mp4");

// combineVideos(
//   "./media/source/10mb.mp4",
//   "./media/source/2mb.mp4",
//   "./media/destination/merge2.mp4"
// );

// addAudio(
//   "./media/source/10mb.mp4",
//   "./media/source/audio1mb.mp3",
//   "./media/destination/addAudio.mp4"
// );

// addImage(
//   "./localMediaFiles/source/4K NATURE -RELAXING VIDEO FOR 4K OLED TV (1).mp4",
//   "./localMediaFiles/source/rings.svg",
//   "./localMediaFiles/output/svgAdded.mp4",
//   1,
//   5
// );

// addText(
//   "./media/source/2mb.mp4",
//   "Hello Filter Size World",
//   "./media/destination/text.mp4",
//   2,
//   3
// );

// trimVideo(
//   "./localMediaFiles/source/Nature Makes You Happy  BBC Earth.mp4",
//   80,
//   10,
//   "./localMediaFiles/output/Nature Makes You Happy  BBC Earth (5).mp4"
// );

// resizingVideo(
//   "./localMediaFiles/source/405x720.mp4",
//   "720x720",
//   "./localMediaFiles/output/720x720.mp4"
// );

// videoScale(
//   "./localMediaFiles/source/visa.mp4",
//   720,
//   720,
//   "./localMediaFiles/output/720x720 scale.mp4"
// );

// aspectVideo(
//   "./localMediaFiles/source/visa.mp4",
//   "720x?",
//   "1:1",
//   "./localMediaFiles/output/aspec1.1.mp4"
// );

// videoSpeed(
//   "./localMediaFiles/source/4K NATURE -RELAXING VIDEO FOR 4K OLED TV (4).mp4",
//   0.25,
//   "./localMediaFiles/output/speed.mp4"
// );

// videoCrop(
//   "./localMediaFiles/output/crop1080.mp4",
//   1920,
//   1080,
//   "./localMediaFiles/output/a1920x1080.mp4"
// );
videoCropAndScale(
  "./localMediaFiles/output/854x480.mp4",
  9,
  16,
  "./localMediaFiles/output/a1920x1080.mp4"
);

// videoFade(
//   "./localMediaFiles/source/4K NATURE -RELAXING VIDEO FOR 4K OLED TV.mp4",
//   5,
//   "./localMediaFiles/output/fade.mp4"
// );

// videoBrightness(
//   "./localMediaFiles/source/4K NATURE -RELAXING VIDEO FOR 4K OLED TV (3).mp4",
//   -0.1,
//   "./localMediaFiles/output/brightness.mp4"
// );

// videoBlur(
//   "./localMediaFiles/source/4K NATURE -RELAXING VIDEO FOR 4K OLED TV (3).mp4",
//   20,
//   "./localMediaFiles/output/blur.mp4"
// );

// videoOverlay(
//   "./localMediaFiles/source/visa.mp4",
//   "./localMediaFiles/source/a.mov",
//   "./localMediaFiles/output/overlayAA.mp4"
// );

// getDimentions(
//   "./localMediaFiles/source/Main Badhiya Tu Bhi Badhiya - Songs.pk - 320Kbps.mp3"
// );
